﻿using System;
using Newtonsoft.Json;
namespace ufs
{
    public class UFSResult
    {
        public string FileUrl { get; set; }
        public bool Success { get; set; }
        public string Msg { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
            //return "{\"success\":" + Success.ToString().ToLower() + ",\"msg\":\"" + Msg + "\",\"fileUrl\":\"" + FileUrl + "\"}";
        }
    }
}
